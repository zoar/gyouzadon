# 餃子丼
餃子丼はトゥートするためだけのマストドンクライアントです。

![screenshot](./screenshot.png)

## 作成環境について
Visual Studio 2015 Update 3 で作成しました。
次のライブラリに依存しています。

+ RestSharp
+ Newtonsoft.Json

## 使い方
### Visual Studio 派
Git と NuGet の使える状態でコードを取ってきてソリューションファイルをダブルクリックすれば開くのでビルドできると思います。

###Visual Studio は使わない派
MSBuild, Git, Nuget(コマンドライン版)へのパスを通してあれば PowerShell からビルドできる*かも*しれません。

```
Invoke-WebRequest -Uri https://bitbucket.org/zoar/gyouzadon/raw/0e358b1daf944b1bebbf491be09ea9d8d5d948e8/build.ps1 -OutFile build.ps1
```

## 実行
`GyouzaDon.exe` を実行すると動きます。
実行ファイルと同じディレクトリに `clientkey.json` や `token.json` が無い場合にはなんとか作ろうと努力するためのフォームが表示されます。
インスタンスにクライアントを登録してアクセストークンを取得できればトゥートできるようになるはずです。

ライセンスは NYSL Version 0.9982 で公開します。

----
2017 K'z Minor Release - Zoar