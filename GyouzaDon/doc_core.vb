﻿Imports System.IO
Imports RestSharp
Imports Newtonsoft.Json
Module doc_core
   Structure ConfDir
      Dim ConfDir As String
      Dim ConfJson As String
      Dim TokenJson As String
   End Structure
   Function StrDir() As ConfDir
      StrDir.ConfDir = Directory.GetCurrentDirectory()
      StrDir.ConfJson = StrDir.ConfDir + "\clientkey.json"
      StrDir.TokenJson = StrDir.ConfDir + "\token.json"
   End Function

   Private Setting_Form As New setting()
   Private Toot_Form As New toot_form()

   Sub Check_token()
      If File.Exists(StrDir.TokenJson) = False Then
         Setting_Form.Show()
      End If

   End Sub

   Sub Client_Regist(ByVal Instance As String, ByVal Client_Name As String)

      If Left(Instance, 4) <> "http" Then
         Instance = "https://" + Instance + "/"
      End If

      If Right(Instance, 1) <> "/" Then
         Instance += "/"
      End If

      Dim Client As New RestClient(Instance)
      Dim Request As New RestRequest("api/v1/apps", Method.POST)

      Dim body As New Hashtable

      body.Add("client_name", Client_Name)
      body.Add("redirect_uris", "urn:ietf:wg:oauth:2.0:oob")
      body.Add("scopes", "write")

      Dim Payload As String
      Payload = JsonConvert.SerializeObject(body, Formatting.Indented).ToString()

      Request.AddHeader("ContentType", "application/x-www-form-urlencoded")
      Request.AddParameter("application/json", Payload, ParameterType.RequestBody)

      Dim ResponseHash As IRestResponse = Client.Execute(Request)
      Dim Message As String
      Dim Response As Object

      Response = JsonConvert.DeserializeObject(ResponseHash.Content)

      Message = "ID(not Client_ID):" + Response("id").ToString() + vbCrLf
      Message += "ClientName:" + Client_Name + vbCrLf
      Message += "Client ID:" + Response("client_id") + vbCrLf
      Message += "Client_Secret:" + Response("client_secret")

      Setting_Form.TextBox3.Text = Message

      Response.add("instance", Instance)

      Dim Write_Stream As StreamWriter

      Write_Stream = New StreamWriter(StrDir.ConfJson, False)
      Write_Stream.Write(JsonConvert.SerializeObject(Response))
      Write_Stream.Close()

      Write_Stream = Nothing
      Response = Nothing
      Request = Nothing
      Client = Nothing

   End Sub

   Sub Get_Token(ByVal UserName As String, ByVal Password As String)
      Dim App_Info As Object
      Dim Read_Stream As StreamReader
      Dim Write_Stream As StreamWriter
      Dim Instance As String
      Dim Client_ID As String
      Dim Client_Secret As String

      Read_Stream = New StreamReader(StrDir.ConfJson)
      App_Info = JsonConvert.DeserializeObject(Read_Stream.ReadToEnd)
      Instance = App_Info("instance")
      Client_ID = App_Info("client_id")
      Client_Secret = App_Info("client_secret")

      Dim Client As New RestClient(Instance)
      Dim Request As New RestRequest("oauth/token", Method.POST)

      Dim body As New Hashtable

      body.Add("client_id", Client_ID)
      body.Add("client_secret", Client_Secret)
      body.Add("grant_type", "password")
      body.Add("username", UserName)
      body.Add("password", Password)
      body.Add("scope", "write")

      Dim Payload As String

      Payload = JsonConvert.SerializeObject(body, Formatting.Indented).ToString()

      Request.AddHeader("ContentType", "application/x-www-form-urlencoded")
      Request.AddParameter("application/json", Payload, ParameterType.RequestBody)

      Dim ResponseHash As IRestResponse = Client.Execute(Request)
      Dim Response As Object

      Response = JsonConvert.DeserializeObject(ResponseHash.Content)

      If ResponseHash.ContentLength = 0 Then
         Setting_Form.TextBox4.Text = "ERROR:ログイン失敗"
      Else
         Setting_Form.TextBox4.Text = "Token:" + Response("access_token")
      End If

      Write_Stream = New StreamWriter(StrDir.TokenJson, False)
      Write_Stream.Write(JsonConvert.SerializeObject(Response))
      Write_Stream.Close()

      Setting_Form.TextBox6.Text += vbCrLf + "Token保存完了"

      Setting_Form.Button3.Enabled = True

      Write_Stream = Nothing
      Read_Stream = Nothing
      Response = Nothing
      body = Nothing
      Request = Nothing
      Client = Nothing


   End Sub

   Sub Post_Toot(ByVal toot As String)
      Dim Token As String
      Dim Read_Stream As StreamReader
      Dim Token_Object As Object
      Dim Instance As String
      Dim App_Info As Object

      Read_Stream = New StreamReader(StrDir.TokenJson)
      Token_Object = JsonConvert.DeserializeObject(Read_Stream.ReadToEnd)
      Token = Token_Object("access_token")

      Read_Stream = New StreamReader(StrDir.ConfJson)
      App_Info = JsonConvert.DeserializeObject(Read_Stream.ReadToEnd)
      Instance = App_Info("instance")

      If toot.Length < 3 Then
         MsgBox("トゥートが短すぎません?")
         Toot_Form.TextBox1.Text = ""
         Exit Sub
      End If

      Dim Toot_hash As New Hashtable
      Toot_hash("status") = toot
      Toot_hash("visibility") = "public"

      Dim Payload As String
      Payload = JsonConvert.SerializeObject(Toot_hash, Formatting.Indented).ToString()

      Dim Client As New RestClient(Instance)
      Dim Request As New RestRequest("api/v1/statuses", Method.POST)

      Request.AddHeader("Authorization", "Bearer " + Token)
      Request.AddHeader("ContentType", "application/x-www-form-urlencoded")

      Request.AddParameter("application/json", Payload, ParameterType.RequestBody)

      Client.Execute(Request)

      Read_Stream = Nothing
      Toot_hash = Nothing
      Client = Nothing
      Request = Nothing
      'Response = Nothing


   End Sub

   Sub Check_Command(ByVal toot As String)
      If (toot = "\hide") Or (toot = "\quit") Or (toot = "\exit") Or (toot = "\bye") Then
         Application.Exit()
      Else
         Call Post_Toot(toot)
      End If
   End Sub

End Module
