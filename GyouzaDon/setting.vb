﻿Imports GyouzaDon.doc_core

Public Class setting
   Sub setting_Load() Handles Me.Load
      Me.Button1.Enabled = False
      Me.Button2.Enabled = False
   End Sub

   Private Sub Activate_Regist() Handles TextBox1.TextChanged
      If TextBox2.Text <> "" Then
         Me.Button1.Enabled = True
      End If
   End Sub

   Private Sub Check_TextBox6() Handles TextBox6.TextChanged
      Call Activate_Login()
   End Sub

   Private Sub Check_TextBox5() Handles TextBox5.TextChanged
      Call Activate_Login()
   End Sub
   Private Sub Activate_Login()
      If (TextBox6.Text <> "") And (TextBox5.Text <> "") Then
         Me.Button2.Enabled = True
      End If
   End Sub

   Private Sub App_Regist() Handles Button1.Click
      Call Client_Regist(TextBox1.Text, TextBox2.Text)
   End Sub

   Private Sub Login_User() Handles Button2.Click
      Call Get_Token(TextBox6.Text, TextBox5.Text)
   End Sub

   Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
      Me.Close()
   End Sub
End Class