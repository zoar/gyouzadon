﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class setting
   Inherits System.Windows.Forms.Form

   'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      Try
         If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
         End If
      Finally
         MyBase.Dispose(disposing)
      End Try
   End Sub

   'Windows フォーム デザイナーで必要です。
   Private components As System.ComponentModel.IContainer

   'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
   'Windows フォーム デザイナーを使用して変更できます。  
   'コード エディターを使って変更しないでください。
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
      Me.Label1 = New System.Windows.Forms.Label()
      Me.TextBox1 = New System.Windows.Forms.TextBox()
      Me.TextBox2 = New System.Windows.Forms.TextBox()
      Me.Label2 = New System.Windows.Forms.Label()
      Me.Button1 = New System.Windows.Forms.Button()
      Me.GroupBox1 = New System.Windows.Forms.GroupBox()
      Me.TextBox3 = New System.Windows.Forms.TextBox()
      Me.GroupBox2 = New System.Windows.Forms.GroupBox()
      Me.TextBox4 = New System.Windows.Forms.TextBox()
      Me.Button2 = New System.Windows.Forms.Button()
      Me.Label3 = New System.Windows.Forms.Label()
      Me.TextBox5 = New System.Windows.Forms.TextBox()
      Me.TextBox6 = New System.Windows.Forms.TextBox()
      Me.Label4 = New System.Windows.Forms.Label()
      Me.Button3 = New System.Windows.Forms.Button()
      Me.GroupBox1.SuspendLayout()
      Me.GroupBox2.SuspendLayout()
      Me.SuspendLayout()
      '
      'Label1
      '
      Me.Label1.AutoSize = True
      Me.Label1.Location = New System.Drawing.Point(6, 15)
      Me.Label1.Name = "Label1"
      Me.Label1.Size = New System.Drawing.Size(120, 12)
      Me.Label1.TabIndex = 0
      Me.Label1.Text = "Instance(e.g. mstdn.jp):"
      '
      'TextBox1
      '
      Me.TextBox1.Location = New System.Drawing.Point(8, 30)
      Me.TextBox1.Name = "TextBox1"
      Me.TextBox1.Size = New System.Drawing.Size(258, 19)
      Me.TextBox1.TabIndex = 1
      '
      'TextBox2
      '
      Me.TextBox2.Location = New System.Drawing.Point(272, 30)
      Me.TextBox2.Name = "TextBox2"
      Me.TextBox2.Size = New System.Drawing.Size(258, 19)
      Me.TextBox2.TabIndex = 3
      Me.TextBox2.Text = "餃子丼"
      '
      'Label2
      '
      Me.Label2.AutoSize = True
      Me.Label2.Location = New System.Drawing.Point(270, 15)
      Me.Label2.Name = "Label2"
      Me.Label2.Size = New System.Drawing.Size(70, 12)
      Me.Label2.TabIndex = 2
      Me.Label2.Text = "Client Name:"
      '
      'Button1
      '
      Me.Button1.Location = New System.Drawing.Point(421, 55)
      Me.Button1.Name = "Button1"
      Me.Button1.Size = New System.Drawing.Size(109, 24)
      Me.Button1.TabIndex = 4
      Me.Button1.Text = "App Registration"
      Me.Button1.UseVisualStyleBackColor = True
      '
      'GroupBox1
      '
      Me.GroupBox1.Controls.Add(Me.TextBox3)
      Me.GroupBox1.Controls.Add(Me.Button1)
      Me.GroupBox1.Controls.Add(Me.Label1)
      Me.GroupBox1.Controls.Add(Me.TextBox2)
      Me.GroupBox1.Controls.Add(Me.TextBox1)
      Me.GroupBox1.Controls.Add(Me.Label2)
      Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
      Me.GroupBox1.Name = "GroupBox1"
      Me.GroupBox1.Size = New System.Drawing.Size(542, 165)
      Me.GroupBox1.TabIndex = 5
      Me.GroupBox1.TabStop = False
      Me.GroupBox1.Text = "Register application"
      '
      'TextBox3
      '
      Me.TextBox3.Location = New System.Drawing.Point(8, 93)
      Me.TextBox3.Multiline = True
      Me.TextBox3.Name = "TextBox3"
      Me.TextBox3.ReadOnly = True
      Me.TextBox3.Size = New System.Drawing.Size(521, 57)
      Me.TextBox3.TabIndex = 5
      '
      'GroupBox2
      '
      Me.GroupBox2.Controls.Add(Me.TextBox4)
      Me.GroupBox2.Controls.Add(Me.Button2)
      Me.GroupBox2.Controls.Add(Me.Label3)
      Me.GroupBox2.Controls.Add(Me.TextBox5)
      Me.GroupBox2.Controls.Add(Me.TextBox6)
      Me.GroupBox2.Controls.Add(Me.Label4)
      Me.GroupBox2.Location = New System.Drawing.Point(12, 183)
      Me.GroupBox2.Name = "GroupBox2"
      Me.GroupBox2.Size = New System.Drawing.Size(542, 165)
      Me.GroupBox2.TabIndex = 6
      Me.GroupBox2.TabStop = False
      Me.GroupBox2.Text = "Login to instance"
      '
      'TextBox4
      '
      Me.TextBox4.Location = New System.Drawing.Point(8, 93)
      Me.TextBox4.Multiline = True
      Me.TextBox4.Name = "TextBox4"
      Me.TextBox4.ReadOnly = True
      Me.TextBox4.Size = New System.Drawing.Size(521, 57)
      Me.TextBox4.TabIndex = 5
      '
      'Button2
      '
      Me.Button2.Location = New System.Drawing.Point(456, 55)
      Me.Button2.Name = "Button2"
      Me.Button2.Size = New System.Drawing.Size(74, 24)
      Me.Button2.TabIndex = 4
      Me.Button2.Text = "Get Token"
      Me.Button2.UseVisualStyleBackColor = True
      '
      'Label3
      '
      Me.Label3.AutoSize = True
      Me.Label3.Location = New System.Drawing.Point(6, 15)
      Me.Label3.Name = "Label3"
      Me.Label3.Size = New System.Drawing.Size(58, 12)
      Me.Label3.TabIndex = 0
      Me.Label3.Text = "Username:"
      '
      'TextBox5
      '
      Me.TextBox5.Location = New System.Drawing.Point(272, 30)
      Me.TextBox5.Name = "TextBox5"
      Me.TextBox5.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
      Me.TextBox5.Size = New System.Drawing.Size(258, 19)
      Me.TextBox5.TabIndex = 3
      '
      'TextBox6
      '
      Me.TextBox6.Location = New System.Drawing.Point(8, 30)
      Me.TextBox6.Name = "TextBox6"
      Me.TextBox6.Size = New System.Drawing.Size(258, 19)
      Me.TextBox6.TabIndex = 1
      '
      'Label4
      '
      Me.Label4.AutoSize = True
      Me.Label4.Location = New System.Drawing.Point(270, 15)
      Me.Label4.Name = "Label4"
      Me.Label4.Size = New System.Drawing.Size(56, 12)
      Me.Label4.TabIndex = 2
      Me.Label4.Text = "Password:"
      '
      'Button3
      '
      Me.Button3.Enabled = False
      Me.Button3.Location = New System.Drawing.Point(467, 354)
      Me.Button3.Name = "Button3"
      Me.Button3.Size = New System.Drawing.Size(74, 24)
      Me.Button3.TabIndex = 6
      Me.Button3.Text = "Close"
      Me.Button3.UseVisualStyleBackColor = True
      '
      'setting
      '
      Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
      Me.ClientSize = New System.Drawing.Size(566, 387)
      Me.Controls.Add(Me.Button3)
      Me.Controls.Add(Me.GroupBox2)
      Me.Controls.Add(Me.GroupBox1)
      Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
      Me.Name = "setting"
      Me.Text = "Setting"
      Me.GroupBox1.ResumeLayout(False)
      Me.GroupBox1.PerformLayout()
      Me.GroupBox2.ResumeLayout(False)
      Me.GroupBox2.PerformLayout()
      Me.ResumeLayout(False)

   End Sub

   Friend WithEvents Label1 As Label
   Friend WithEvents TextBox1 As TextBox

   Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

   End Sub

   Friend WithEvents TextBox2 As TextBox
   Friend WithEvents Label2 As Label
   Friend WithEvents Button1 As Button
   Friend WithEvents GroupBox1 As GroupBox

   Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

   End Sub
   Friend WithEvents GroupBox2 As GroupBox
   Friend WithEvents Button2 As Button
   Friend WithEvents Label3 As Label
   Friend WithEvents TextBox5 As TextBox
   Friend WithEvents TextBox6 As TextBox
   Friend WithEvents Label4 As Label
   Friend WithEvents Button3 As Button
   Friend WithEvents TextBox3 As TextBox
   Friend WithEvents TextBox4 As TextBox
End Class
