﻿Imports GyouzaDon.doc_core
Public Class toot_form
   Private Sub toot_form_Load(sender As Object, e As EventArgs) Handles Me.Load
      Me.FormBorderStyle = FormBorderStyle.None
      Call Check_token()
   End Sub

   Private Sub TextBox1_KeyPress(sender As Object, ByVal e As KeyPressEventArgs) Handles TextBox1.KeyPress
      If e.KeyChar = Chr(13) Then
         Call Check_Command(TextBox1.Text)
         TextBox1.Text = ""
         e.KeyChar = ""
      End If
   End Sub

   'タイトルバーが無い状態でもグレー部分をドラッグすることで移動できるようにする
   Private MousePoint As Point

   Private Sub Form1_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown
      If (e.Button And MouseButtons.Left) = MouseButtons.Left Then
         MousePoint = New Point(e.X, e.Y)
      End If
   End Sub

   Private Sub Form1_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
      If (e.Button And MouseButtons.Left) = MouseButtons.Left Then
         Me.Left += e.X - MousePoint.X
         Me.Top += e.Y - MousePoint.Y
      End If
   End Sub

End Class
